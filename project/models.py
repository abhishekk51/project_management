from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class CommonColumnModel(models.Model):
    slug = models.SlugField(max_length=200, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Project(CommonColumnModel):

    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    assigned_users = models.ManyToManyField(User, related_name='project_assigned')
    deadline = models.DateField()
    status = models.BooleanField(default=True)

    class Meta:
        ordering = ['name']
        db_table = 'project'

    def __str__(self):
        return self.name


class Task(CommonColumnModel):
    CREATED = 'created'
    IN_PROGRESS = 'in_progress'
    TO_VERIFY = 'to_verify'
    READY_TO_RELEASE = 'ready_to_release'
    RELEASED = 'released'

    STATUS = (
        (CREATED, 'CREATED'),
        (IN_PROGRESS, 'IN PROGRESS'),
        (TO_VERIFY, 'TO VERIFY'),
        (READY_TO_RELEASE, 'READY TO RELEASE'),
        (RELEASED, 'RELEASED'),
    )

    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=80)
    description = models.TextField(null=True, blank=True)
    assigned_users = models.ManyToManyField(User, related_name='task_assigned')
    status = models.CharField(max_length=20, choices=STATUS, default=CREATED)

    class Meta:
        db_table = 'task'
        ordering = ['project', 'name']

    def __str__(self):
        return self.name
