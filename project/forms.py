from datetime import date

from django import forms
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from .models import Task
from .models import Project
from django.contrib.auth.models import User


def no_past_date(value):
    today = date.today()
    if value < today:
        raise ValidationError('Deadline Date cannot be a past date.')


class ProjectRegisterForm(forms.ModelForm):
    deadline = forms.DateField(help_text='Format dd-mm-yyyy', input_formats=['%d-%m-%Y', '%Y-%m-%d'], validators=[no_past_date])

    class Meta:
        model = Project
        exclude = ('slug', 'created', 'updated')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def save(self, commit=True):
        project = super().save(commit=False)
        project.slug = slugify(str(self.cleaned_data['name']))
        project.save()
        self.save_m2m()
        return project


class TaskRegisterForm(forms.ModelForm):

    class Meta:
        model = Task
        exclude = ('slug', 'created', 'updated')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def save(self, commit=True):
        project = super().save(commit=False)
        project.slug = slugify(str(self.cleaned_data['name']))
        project.save()
        self.save_m2m()
        return project

