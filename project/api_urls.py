from django.urls import path
from . import apis


urlpatterns = [
    path('view-project', apis.ProjectListApi.as_view()),
    path('view-project/<int:pk>', apis.ProjectDetailApi.as_view()),
    path('delete-project/<int:pk>', apis.ProjectDeleteApi.as_view()),
    path('view-task', apis.TaskListApi.as_view()),
    path('view-task/<int:pk>', apis.TaskDetailApi.as_view()),
    path('delete-task/<int:pk>', apis.TaskDeleteApi.as_view()),
]