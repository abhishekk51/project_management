from datetime import date

from django.core.exceptions import ValidationError
from rest_framework import serializers

from .models import Task
from .models import Project


def no_past_date(value):
    today = date.today()
    if value < today:
        raise ValidationError('Deadline Date cannot be a past date.')


class ProjectRegisterSerializer(serializers.ModelSerializer):
    deadline = serializers.DateField(help_text='Format dd-mm-yyyy', input_formats=['%d-%m-%Y', '%Y-%m-%d'], validators=[no_past_date])
    assigned_users = serializers.SerializerMethodField()

    class Meta:
        model = Project
        exclude = ('slug', 'created', 'updated')

    def get_assigned_users(self, obj):
        users_list = []
        for user_obj in obj.assigned_users.all():
            if user_obj.first_name:
                users_list.append("{first_name} {last_name}".format(first_name=user_obj.first_name, last_name=user_obj.last_name))
        return users_list


class TaskRegisterSerializer(serializers.ModelSerializer):
    assigned_users = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()

    class Meta:
        model = Task
        exclude = ('slug', 'created', 'updated')

    def get_assigned_users(self, obj):
        users_list = []
        for user_obj in obj.assigned_users.all():
            if user_obj.first_name:
                users_list.append("{first_name} {last_name}".format(first_name=user_obj.first_name, last_name=user_obj.last_name))
        return users_list

    def get_project(self, obj):
        return obj.project.name
