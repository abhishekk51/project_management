# Create your views here.
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from project.forms import TaskRegisterForm, ProjectRegisterForm
from project.models import Project, Task


class ProjectList(ListView):
    model = Project
    paginate_by = 3
    context_object_name = 'projects'
    template_name = 'project/project-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CreateProject(CreateView):
    model = Project
    form_class = ProjectRegisterForm
    context_object_name = 'form'
    template_name = 'project/project-add.html'
    success_url = '/view-projects'


class UpdateProject(UpdateView):
    model = Project
    form_class = ProjectRegisterForm
    context_object_name = 'form'
    template_name = 'project/project-add.html'
    success_url = '/view-projects'


class DeleteProject(DeleteView):
    model = Project
    success_url = '/view-projects'


class TaskList(ListView):
    model = Task
    paginate_by = 100
    context_object_name = 'tasks'
    template_name = 'project/task-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CreateTask(CreateView):
    model = Task
    form_class = TaskRegisterForm
    context_object_name = 'form'
    template_name = 'project/task-add.html'
    success_url = '/view-tasks'


class UpdateTask(UpdateView):
    model = Task
    form_class = TaskRegisterForm
    context_object_name = 'form'
    template_name = 'project/task-add.html'
    success_url = '/view-tasks'


class DeleteTask(DeleteView):
    model = Task
    success_url = '/view-tasks'
