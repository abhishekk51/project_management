from rest_framework.generics import ListAPIView, RetrieveAPIView, DestroyAPIView

from project.helpers import CustomPagination
from project.serializers import ProjectRegisterSerializer, TaskRegisterSerializer


class ProjectListApi(ListAPIView):
    serializer_class = ProjectRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.filter(status=True)
    pagination_class = CustomPagination


class ProjectDetailApi(RetrieveAPIView):
    serializer_class = ProjectRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()


class ProjectDeleteApi(DestroyAPIView):
    serializer_class = ProjectRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()

    def perform_destroy(self, instance):
        instance.status = False
        instance.save()


class TaskListApi(ListAPIView):
    serializer_class = TaskRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()
    pagination_class = CustomPagination


class TaskDetailApi(RetrieveAPIView):
    serializer_class = TaskRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()


class TaskDeleteApi(DestroyAPIView):
    serializer_class = TaskRegisterSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()

