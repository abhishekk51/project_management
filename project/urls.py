from django.urls import path
from . import views


urlpatterns = [
    path('view-projects', views.ProjectList.as_view()),
    path('add-project/', views.CreateProject.as_view()),
    path('edit-project/<slug>', views.UpdateProject.as_view()),
    path('delete-project/<slug>', views.DeleteProject.as_view()),
    path('view-tasks', views.TaskList.as_view()),
    path('add-task/', views.CreateTask.as_view()),
    path('edit-task/<slug>', views.UpdateTask.as_view()),
    path('delete-task/<slug>', views.DeleteTask.as_view()),
]