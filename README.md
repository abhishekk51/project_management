# PROJECT MANAGEMENT ASSIGNMENT
 
## Installation
pip3 install -r requirements.txt

## For Creating User
python3 manage.py createsuperuser  

## Start the django server
python3 manage.py runserver  

## Django Assignment

### Project listing page
URL: http://127.0.0.1:8000/view-projects  

PS: rest of the links can be find there  

### Task listing page
URL: http://127.0.0.1:8000/view-tasks  

PS: rest of the links can be find there  


## DRF Assignment

### project listing api
URL: http://127.0.0.1:8000/api/v1/view-project  
Request Type: GET  
Content-Type: application/json  

### View single project
URL: http://127.0.0.1:8000/api/v1/view-project/<int:pk>  
Request Type: GET  
Content-Type: application/json  

### Delete project
URL: http://localhost:8000/api/v1/delete-project/<int:pk>  
Request Type: DELETE  

### Task listing api
URL: http://127.0.0.1:8000/api/v1/view-task  
Request Type: GET  
Content-Type: application/json  

### View single task
URL: http://127.0.0.1:8000/api/v1/view-task/<int:pk>    
Request Type: GET  
Content-Type: application/json  

### Delete task
URL: http://localhost:8000/api/v1/delete-task/<int:pk>   
Request Type: DELETE  


#### can add dummy content from admin panel  
URL: http://localhost:8000/admin/  



